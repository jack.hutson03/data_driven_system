<?PHP

function doughnutGetDataFromArray($zipArray,$myterm) {
		$datainr =  array();

		$nterm = "+".str_replace(" ","+",$myterm);
		$nterm =  addslashes($nterm) . '*';
		
		foreach ($zipArray as $key => $value) {
			$sql = "SELECT * FROM paid WHERE zip = ".$key." and MATCH (
				companyname, name, lname, 
				cat1, cat2, cat3, subcat1, 
				subcat2, subcat3, subcat4, 
				service1, service2, service3 
			) AGAINST ( :term IN BOOLEAN MODE ) ";

			$datainr1 = R::getAll( $sql,  [':term' => $nterm ]  );

			$datainr = array_merge($datainr, $datainr1);
		}
		shuffle ($datainr);
		$results['paid'] = $datainr;





		foreach ($zipArray as $key => $value) {

			if (memory_get_usage() > 15000000 ) { break; }
			$testsql = '
				SELECT count(*) as hasdata 
				FROM information_schema.TABLES 
				WHERE (TABLE_SCHEMA = \'locoolly1\') 
				AND (TABLE_NAME = \'ziptbl' . $key . '\')';

			$test = R::getAll( $testsql );

			if ($test[0]['hasdata'] == 1 ){
				$TBLNAME =  'ziptbl' .$key;
				$sql = "SELECT * FROM $TBLNAME WHERE MATCH (
					companyname, name, lname, 
					cat1, cat2, cat3, subcat1, 
					subcat2, subcat3, subcat4, 
					service1, service2, service3 
				) AGAINST ( :term IN BOOLEAN MODE )";
				//$nterm =  addslashes($myterm) . '*';
				$datainr2 = R::getAll( $sql,  [':term' => $nterm ]   );
				shuffle ($datainr2);
				$results[$key] = $datainr2;
			}
		}


		foreach ($results as $key => $value) {
			foreach ($value as $kk => $vv) {

				$bigstr = $vv['companyname'] . ' ' . $vv['name'] . ' ' . $vv['lname'] . ' ' . 
						  $vv['cat1'] . ' ' . $vv['cat2'] . ' ' . $vv['cat3'] . ' ' . 
						  $vv['subcat1'] . ' ' . 	$vv['subcat2']  . ' ' . $vv['subcat3']  . ' ' . $vv['subcat4']  . ' ' . 
						  $vv['service1'] . ' ' . $vv['service2'] . ' ' . $vv['service3'];
				
				$needles = explode(" ",$myterm);
				if (contains_all_s($bigstr, $needles)) {
					$data_results[] = $vv;
				}

			}
		}
		return $data_results;
}


function contains_all_s($str,array $words) {
    if(!is_string($str))
        { return false; }
    return count(
        array_intersect(
        #   lowercase all words
            array_map('strtolower',$words),
        #   split by non-alphanumeric chars (hyphens are safe)
            preg_split('/[^a-z0-9\-]/',strtolower($str))
        )
    ) == count($words);
}

function  doughnutZipCodeList($data) {
 	
	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////
	//	$data['min'] 	= $_REQUEST['m'];
	//	$data['miles'] 	= $_REQUEST['m'];
	//	$data['zip']  	= $_REQUEST['z'];
	$innerRad = (int)$data['min'];
	if ($innerRad == '') {$innerRad = 5;}

	$rad = $data['miles'];
	$datain = R::getAll( 'SELECT * FROM azips where ZipCode = '. $data['zip'].'  limit 1;' );
	
	$lat 	=  $datain[0]['Latitude'] ;
	$lng 	=  $datain[0]['Longitude'] ;
	$R 		= 	3959;  // earth mean radius in MILES
	$maxLat = $lat + rad2deg($rad/$R);
	$minLat = $lat - rad2deg($rad/$R);
	$maxLon = $lng + rad2deg($rad/$R/cos(deg2rad($lat)));
	$minLon = $lng - rad2deg($rad/$R/cos(deg2rad($lat)));

	$s1 = 'Select * From azips Where Latitude Between '.$minLat.' And '.$maxLat.' And Longitude Between '.$minLon.' And '.$maxLon;
	$datazone = R::getAll( $s1 );
	
	$lat1 = $lat;
	$lon1 = $lng;
	foreach ($datazone as $key => $value) {
		$zip = $value['ZipCode'];
		$lat2 = $value['Latitude'];
		$lon2 = $value['Longitude'];
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		// $value['dst'] = $miles;
		$distAll[$zip] = $miles;
	}
	asort($distAll);

	////////////////////////////////////////////////////////
	// assumes ring size of five
	////////////////////////////////////////////////////////

	$center_pie = intval($data['miles']) - $innerRad;
	foreach($distAll as $k => $v){
		if ($v >= $center_pie) {	
			$effZips[$k] = $v; 
		}
	}

	$cluster['zip_by_miles'] = $distAll;
	$cluster['zip_code_list1'] = implode(',', array_keys($distAll));
	$cluster['zip_code_list2'] = implode(',', array_keys($effZips));
	$cluster['zip_array'] = $effZips;
	
	return $cluster;
}

function  fullTextSearchDataCall($data) {
	$datain = R::getAll( 'SELECT * FROM raww WHERE MATCH (
			companyname, fname, lname, 
			cat1, cat2, cat3, subcat1, 
			subcat2, subcat3, subcat4, 
			service1, service2, service3 
		) AGAINST (:term IN BOOLEAN MODE )',  [':term' => $data['myterm'] ]   );

		$results = $datain;
		return $results;
}

function  fullTextZipCodeMatch($data) {
	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////
	//	$data['miles'] = $_REQUEST['m'];
	//	$data['zip']  = $_REQUEST['z'];
	//  $data['myterm']  = $_REQUEST['s'];
	$rad = $data['miles'];
	$datain = R::getAll( 'SELECT * FROM azips where ZipCode = '. $data['zip'].'  limit 1;' );
	
	$lat 	=  $datain[0]['Latitude'] ;
	$lng 	=  $datain[0]['Longitude'] ;

	$R 		= 	3959;  // earth mean radius in MILES

	$maxLat = $lat + rad2deg($rad/$R);
	$minLat = $lat - rad2deg($rad/$R);

	$maxLon = $lng + rad2deg($rad/$R/cos(deg2rad($lat)));
	$minLon = $lng - rad2deg($rad/$R/cos(deg2rad($lat)));

	$s1 = 'Select * From azips Where Latitude Between '
		.$minLat.' And '
		.$maxLat.' And Longitude Between '
		.$minLon.' And '
		.$maxLon;

	$datazone = R::getAll( $s1 );

	$lat1 = $lat;
	$lon1 = $lng;

	foreach ($datazone as $key => $value) {
		$zip = $value['ZipCode'];
		$lat2 = $value['Latitude'];
		$lon2 = $value['Longitude'];
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;

		$value['dst'] = $miles;
		$distAll[$zip] = $miles;
		$zip_all[$zip] = $value;
		$zipstr .= $zip.', ';
		$ziplist[] = $zip;
	}

	asort($distAll);

	$datainr = array();
		foreach ($distAll as $key => $value) {

			$sql = "SELECT * FROM paid WHERE zip = ".$key." and MATCH (
				companyname, name, lname, 
				cat1, cat2, cat3, subcat1, 
				subcat2, subcat3, subcat4, 
				service1, service2, service3 
			) AGAINST ( :term IN BOOLEAN MODE ) ";
			$nterm = $data['myterm'] .'*';
			$datainr1 = R::getAll( $sql,  [':term' => $nterm ]  );
			$datainr = array_merge($datainr, $datainr1);

		}
		shuffle ($datainr);
		$results['paid'] = $datainr;


		foreach ($distAll as $key => $value) {

			if (memory_get_usage() > 20000000 ) { break; }

			$testsql = '
				SELECT count(*) as hasdata 
				FROM information_schema.TABLES 
				WHERE (TABLE_SCHEMA = \'locoolly1\') 
				AND (TABLE_NAME = \'ziptbl' . $key . '\')';

			$test = R::getAll( $testsql );

		
	
			if ($test[0]['hasdata'] == 1 ){

				$TBLNAME =  'ziptbl' .$key;
				$sql = "SELECT * FROM $TBLNAME WHERE MATCH (
					companyname, name, lname, 
					cat1, cat2, cat3, subcat1, 
					subcat2, subcat3, subcat4, 
					service1, service2, service3 
				) AGAINST ( :term IN BOOLEAN MODE )";
				$nterm =  addslashes($data['myterm']) . '*';
				$datainr2 = R::getAll( $sql,  [':term' => $nterm ]   );
				shuffle ($datainr2);
				$results[$key] = $datainr2;
				#echo '<pre>'; print_r($results[$key] );
			}


		}



	return $results;

}
	
	function dumpArray($array) {
		echo "<table border=1 cellpadding=2 cellspacing=1>\n";
		echo "<tr><th bgcolor=\"#6B86BD\">Variable</th>\n";
		echo "<th bgcolor=\"#6B86BD\">Value</th></tr>\n";
		while (list($key, $val) = each($array)) {
			echo "<tr><td bgcolor=\"#EEEEEE\">$key</td>\n";
			if (is_array($val)) {
				echo "    <td bgcolor=\"#CCCCCC\">\n";
				dumpArray($val);
				echo "</td></tr>";
			} else {
				echo "    <td bgcolor=\"#CCCCCC\"><pre>".htmlspecialchars($val)."</pre></td></tr>\n";
			}
		}
		echo "</table>";
	}

	function rCountArray($array) {
		if (!isset($ret_value)) {$ret_value='';}
		$ret_value .= "<table border=1 cellpadding=2 cellspacing=1>\n";
		$ret_value .= "<tr><th bgcolor=\"#6B86BD\">Variable</th>\n";
		$ret_value .= "<th bgcolor=\"#6B86BD\">Value</th></tr>\n";
		$i=0;
		while (list($key, $val) = each($array)) {
		$i++;
		  $ret_value .= "<tr><td bgcolor=\"#EEEEEE\">".$i."_".$key."</td>\n";
		  if (is_array($val)) {
			$ret_value .= "    <td bgcolor=\"#CCCCCC\">\n";
			$ret_value .= rCountArray($val);
			$ret_value .= "</td></tr>";
		  } else if (is_object($val)) {
			$ret_value .= "    <td bgcolor=\"#CCCCCC\"><textarea cols=30 rows=10>\n";
			$ret_value .= print_r($val,'1');
			$ret_value .= "</textarea></td></tr>";
		  } else {
			$ret_value .= "    <td bgcolor=\"#CCCCCC\"><pre>".htmlspecialchars($val)."</pre></td></tr>\n";
		  }
		}
		$ret_value .= "</table>";
		return $ret_value; 
	}

	function returnArray($array) {
		if (!isset($ret_value)) {$ret_value='';}
		$ret_value .= "<table border=1 cellpadding=2 cellspacing=1>\n";
		$ret_value .= "<tr><th bgcolor=\"#6B86BD\">Variable</th>\n";
		$ret_value .= "<th bgcolor=\"#6B86BD\">Value</th></tr>\n";
		$i=0;
		while (list($key, $val) = each($array)) {
		  $ret_value .= "<tr><td bgcolor=\"#EEEEEE\">$key</td>\n";
		  if (is_array($val)) {
			$ret_value .= "    <td bgcolor=\"#CCCCCC\">\n";
			$ret_value .= returnArray($val);
			$ret_value .= "</td></tr>";
		  } else if (is_object($val)) {
			$ret_value .= "    <td bgcolor=\"#CCCCCC\"><textarea cols=30 rows=10>\n";
			$ret_value .= print_r($val,'1');
			$ret_value .= "</textarea></td></tr>";
		  } else {
			$ret_value .= "    <td bgcolor=\"#CCCCCC\"><pre>".htmlspecialchars($val)."</pre></td></tr>\n";
		  }
		}
		$ret_value .= "</table>";
		return $ret_value; 
	}

	function removeBOM($str=""){
		if(substr($str, 0,3) == pack("CCC",0xef,0xbb,0xbf)) {
			$str=substr($str, 3);
		}
		return $str;
	}

	function spParam($val) {
		$ret_value = str_replace("'", "''", $val);
		return $ret_value;
	}

	function jsParam($val) {
		$ret_value = str_replace("'", "\'", $val);
		return $ret_value;
	}

	function nullOrString($val) {
		if($val == ''){
			$ret_value = 'NULL';
		}
		else{
			$ret_value = "'" . spParam($val) . "'";
		}
		return $ret_value;
	}

	
		
?>
